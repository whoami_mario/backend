FROM node:16.15-alpine
ENV PORT=4000

RUN apk --update --no-cache add curl

WORKDIR /usr/src/app
COPY package.json ./
RUN yarn install

COPY . .

EXPOSE ${PORT}

CMD [ "npm", "start" ]
