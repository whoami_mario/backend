require('dotenv').config();

module.exports = {
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: 5432,
    dialect: 'postgres',
    define: {
      freezeTableName: true,
    },
    useUTC: false,
    dateStrings: true,
    timezone: process.env.TZ,
    migrationStorage: 'sequelize',
    seederStorage: 'sequelize',
  },
};
