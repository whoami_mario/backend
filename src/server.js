const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('dotenv').config();
const os = require('os');
const asyncHandler = require('./middlewares/asyncHandler');
const errorHandler = require('./middlewares/errorHandler');
const noRouteHandler = require('./middlewares/noRouteHandler');
global.__basedir = __dirname;
const models = require('./db/models');

const app = express();
const apiPort = process.env.PORT || 4000;
const apiVer = '/api/v1';

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => {
  res.status(200).json({ status: 'ok' });
});

app.get(`${apiVer}/hostinfo`, (req, res) => {
  res.status(200).json({
    success: true,
    data: {
      hostname: os.hostname(),
      platform: os.platform(),
      release: os.release(),
      arch: os.arch(),
    },
  });
});

app.get(
  `${apiVer}/users/:id`,
  asyncHandler(async (req, res, next) => {
    const { id } = req.params;

    const user = await models.User.findByPk(id, {
      attributes: { exclude: ['id', 'createdAt', 'updatedAt'] },
    });

    if (!user) {
      return next({
        message: 'User not found',
        statusCode: 404,
      });
    }

    res.status(200).json({ success: true, data: user });
  })
);

app.get('*', noRouteHandler);
app.use(errorHandler);

models.sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');

    app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`));
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });
